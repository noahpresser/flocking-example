﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlockManager : MonoBehaviour {

    List<Transform> fishies;

    public bool useAlignment;
    public bool useCohesion;
    public bool useSeparation;

    public float speed = .5f;
    public float RADIUS = 100f;

    // Use this for initialization
    void Start () {;
        fishies = new List<Transform>();
        GameObject[] fishObjs = GameObject.FindGameObjectsWithTag("Fish");
        foreach (GameObject g in fishObjs)
        {
            fishies.Add(g.transform);
        }
	}
	
	// Update is called once per frame
	void Update () {
        foreach (Transform fish in fishies)
        {
            Vector3 alignment = ComputeAlignment(fish);
            Vector3 cohesion = ComputeCohesion(fish);
            Vector3 separation = ComputeSeparation(fish);

            Vector3 newVelocity = fish.forward;
            newVelocity.x += alignment.x + cohesion.x + separation.x;
            newVelocity.y += alignment.y + cohesion.y + separation.y;
            newVelocity.z += alignment.z + cohesion.z + separation.z;

            //set the fish to face in the direction of the new velocity vector
            fish.LookAt(newVelocity);

            //move the fish in the direction it is facing
            fish.position +=  fish.forward* speed;
        }
    }


    //line up with nearby fish
    Vector3 ComputeAlignment(Transform currentfish) {
        if (!useAlignment)
            return Vector3.zero;
        
        Vector3 alignment = new Vector3();
        int neighborCount = 0;
        //loop through all fishies
        foreach (Transform fish in fishies)
        {
            if (fish != currentfish)
            {
                //for all fish within the radius, add VELOCITY and increment neighborCount
                if (Vector3.Distance(fish.position, currentfish.position) < RADIUS)
                {
                    alignment.x += fish.forward.x;
                    alignment.y += fish.forward.y;
                    alignment.z += fish.forward.z;
                    neighborCount++;
                }
            }
        }
        //average all of the computations just made
        alignment.x /= neighborCount;
        alignment.y /= neighborCount;
        alignment.z /= neighborCount;
        Vector3.Normalize(alignment);
        return alignment;
    }

    //steer towards the center of mass of all fish
    Vector3 ComputeCohesion(Transform currentfish) {
        if (!useCohesion)
            return Vector3.zero;

        Vector3 cohesion = new Vector3();
        int neighborCount = 0;
        //loop through all fishies
        foreach (Transform fish in fishies)
        {
            if (fish != currentfish)
            {   
                //for all fish within the radius, add POSITION and increment neighborCount
                if (Vector3.Distance(fish.position, currentfish.position) < RADIUS)
                {
                    cohesion.x += fish.position.x;
                    cohesion.y += fish.position.y;
                    cohesion.z += fish.position.z;
                    neighborCount++;
                }
            }
        }
        //average all of the computations just made
        cohesion.x /= neighborCount;
        cohesion.y /= neighborCount;
        cohesion.z /= neighborCount;

        //now we have the center of mass
        //we want the direction TOWARDS the center of mass
        Vector3 fishPos = currentfish.position;
        cohesion = new Vector3(cohesion.x - fishPos.x, cohesion.y - fishPos.y, cohesion.z - fishPos.z);
        Vector3.Normalize(cohesion);
        return cohesion;
    }

    //steer away from neighbors
    Vector3 ComputeSeparation(Transform currentfish) {
        if (!useSeparation)
            return Vector3.zero;

        Vector3 separation = new Vector3();
        int neighborCount = 0;
        //loop through all fishies
        foreach (Transform fish in fishies)
        {
            if (fish != currentfish)
            {
                //for all fish within the radius, find the direction towards that fish
                if (Vector3.Distance(fish.position, currentfish.position) < RADIUS)
                {
                    separation.x += fish.position.x - currentfish.position.x;
                    separation.y += fish.position.y - currentfish.position.y;
                    separation.z += fish.position.z - currentfish.position.z;
                    neighborCount++;
                }
            }
        }
        //average all of the computations just made
        separation.x /= neighborCount;
        separation.y /= neighborCount;
        separation.z /= neighborCount;

        //negate the direction so we swim away
        separation *= -1;

        Vector3.Normalize(separation);
        return separation;
    }
}
